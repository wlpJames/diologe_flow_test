
import argparse
import qi
import sys

from pep_modules.listener import listener
from pep_modules.app_framework import app_framework

class test_app:
	'''
		This is the main class, your app will start here, 

		Read though, you will filnd the app_route function, this will take pepper_requests (see the docs), and 
		carry out the appropriate task, generaly display something on the tablet or starting a speach routine.

		In this example, requests are either sent from the app_framework (whereby the innitiate in the tablet) or
		they may come from the function -- speech_callback() 
		you can also call the function from anywhere in the code that you like.  

	'''

	def __init__(self, app):

		#set a framework that can organise comunication --- the app route here is a function that will recieve data sent by the tablet
		self.framework = app_framework(app.session, self.app_route)

		#show index/home page on startup
		self.framework.display_page('index.html')
		self.app = app
		self.session = app.session

		#In this example a speach service is initiated that shows how simple vocal speekings can go down
		api = "ALAnimatedSpeech"
		self.AS = self.session.service(api)

		#A module that will listen for vocal commands from users, the callback will recieve all the data recieved from wit.ai (touch the head to start!)
		self.speach_routine = listener(self.app, self.speech_callback) 
		self.speach_routine.start()


	def app_route(self, request):
		'''
			This is a weighty function that takes a request from the tablet and will decide which page/function it refers to and what is asking to be done

			be creative here!! im basing mine of a flask server app.py file, but do what you like.

			see the docs for the request formating
		'''

		#-----------------------------------------------------------------------------------------------
		#
		#										   INDEX PAGE
		#
		#-----------------------------------------------------------------------------------------------
		if request['route'] == 'index':
			
			#in the example, nothing will actually call this, if it did, a form might for example call a POST
			if request['request_type'] == 'POST':
				return


			elif request['request_type'] == 'GET':
				self.framework.display_page("index.html")


		#an example of a call for data with a callback function (callback_id)
		if request['route'] == 'index/JSON':

			if request['request_type'] == 'GET':

				#This is just to show that console messages are redirected
				return_data = {
								'data': 'message recieved',
								'callback_id' : request['callback_id']
							  }

				self.framework.send_to_tablet(return_data)



		#-----------------------------------------------------------------------------------------------
		#
		#										   CONTENT PAGE
		#
		#-----------------------------------------------------------------------------------------------

		#this one is very simple (if somthing sends a get request for a 'page_2', page_2 is displayed on the tablet)
		if request['route'] == 'page_2':

			if request['request_type'] == 'GET': 
				self.framework.display_page("page_2.html")
				self.AS.say('this is an example page')


		
		#-----------------------------------------------------------------------------------------------
		#
		#										Footer for speach route
		#
		#-----------------------------------------------------------------------------------------------

		#the html of the footer is automaticaly included when running the framework.js file.
		#It is possable to alter the html via document.getElementById('footer').innerHTML = 'YOUR_HTML'
		#CSS for the footer can be found in html/styles/theme.CSS 

		if request['route'] == 'footer':

			if request['request_type'] == 'POST':
				#allow an aproval or disaproval of what was said
				print(recieved)


			#do the thing to sent request['data'] to footer.
			#execute js
			js = 'show_in_footer(' + request['data'] + '");'
			print(js)
			self.framework.execute_JS(js)

		return

	def speech_callback(self, data):
		'''
			A function that will be called when the user has asked a question
		'''
		text = '"I heard : ' + data['_text']

		request = {'route' : 'footer', 'request_type' : '' ,'data' : text}

		self.app_route(request)

		#reset the speach routine
		self.speach_routine.reset()


def main():
	#set up pepper session
	parser = argparse.ArgumentParser()
	parser.add_argument("--ip", type=str, default="198.18.0.1",
	                    help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
	parser.add_argument("--port", type=int, default=9559,
	                    help="Naoqi port number")

	args = parser.parse_args()

	try:
	    # Initialize qi framework.
	    connection_url = "tcp://" + args.ip + ":" + str(args.port)
	    app = qi.Application(["Stream_to_wit", "--qi-url=" + connection_url])
	except RuntimeError:
	    print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
	           "Please check your script arguments. Run with -h option for help.")
	    sys.exit(1)

	app.session.connect('192.168.0.116')
	session = app.session
	main = test_app(app)

	#wait for input before quiting
	raw_input('any key to exit')
	main.framework.close()
	main.speach_routine.stop()


if __name__ == "__main__":
	main()






