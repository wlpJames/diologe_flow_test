import qi
import argparse
import sys
import time
import json
import functools
from speach_detect import speach_detect
from pep_stream import Stream_to_wit


class listener(object):
    """
    a class that can take a stream of audio and discern where an instance of speak is started, ongoing or finished

    ...

    Atributes
    -------
    read the code

    Methods
    -------
    def __init__(self, app, callback)

        subscibes to many things

    def start_listening(self):  #this should be renamed
        tells the listener object to start

    def set_listening(Self):
        sets a boolian for the is_listening value and turns
        off anything that might distrupt the mic readings
        will apropriatly set autonomous life acording to the 
        is_listening variable

    def stop_listening(self):
        ends all proccesses

    """

    def __init__(self, app, callback, api_token):
        """
        Parameters
        ----------
            app : qi::application   
            callback : a function that will be called when text has been recognised 
        """

        self.api_token = api_token

        self.session = app.session

        self.callback = callback

        #innit my audio bypass
        self.pep_stream = Stream_to_wit(app, api_token = self.api_token)
        self.session.registerService("Stream_to_wit", self.pep_stream)

        #init audio proccesing class
        self.speach_det = speach_detect(Threshold = 0.2, Time_period = .5)
        self.pep_stream.set_audio_process_obj(self.speach_det)

        #autonomous life
        self.AL = self.session.service("ALAutonomousLife")

        #flags for threads
        self.is_listening = False
        self.is_running = False

        #storage for the input
        self.user_input = None

        #init pepper touch responce
        self.memory = self.session.service("ALMemory")
        super(listener, self).__init__()
        self.touch = self.memory.subscriber("TouchChanged")
        self.touchId = self.touch.signal.connect(functools.partial(self.onTouched, "TouchChanged"))


    def start(self):
        
        #run audio bypass
        self.pep_stream.startProcessing(stream_now = False)

        #start listening for touches

        self.is_running = True


    def set_listening(self, value):
        '''
        sets a boolian for the is_listening value and turns
        off anything that might distrupt the mic readings
        will apropriatly set autonomous life acording to the 
        is_listening variable
        '''
        self.is_listening = value
#/menu/myrobot

    def set_auto_life(self, value):
        self.AL.setAutonomousAbilityEnabled("All", value) #perhaps change this to just AL


    def onTouched(self, strVarName, value):
        """ 
        This will be called each time a touch is detected.
        """
        print(value)
        for p in value:
            if p[1] and p[0] == 'Head':
                print('head touched')
                self.pep_stream.has_been_activated_by_touch = True
                self.on_human_question()


    def on_human_question(self):

        '''
        on_touch and on_human quiestion lead here.
        '''
        
        if self.is_listening == True:
            print('on human input rejected')
            return

        self.set_listening(True)
        self.set_auto_life(False)

        self.pep_stream.start_streaming()
        #call to speach started
        self.speach_det.speach_started()


        #wait for speach to finish
        while True:
            #if speach detect has decided that there is no speech then everthing is cancelled
            if self.speach_det.stage == self.speach_det.aborted:
                print 'has recognised an abort signal'
                self.pep_stream.abort()
                self.speach_det.reset()
                self.set_auto_life(True)
                self.reset()
                return
            #if speach detect determines that speach has finished, continue.
            if (self.speach_det.stage == self.speach_det.finished):
                print('finished detected')
                break


        #reset the speach detect
        self.speach_det.reset()
        #stop accepting data from the microphones, end streaming to wit
        self.pep_stream.stopProcessing()


        #wait for the a-sync responce
        while True:
            if self.pep_stream.responce != None:
                break

        responce = self.pep_stream.getLastResponce().content
        print (responce)

        self.callback(json.loads(responce))

        #resets autonomous life
        self.set_auto_life(True)


    def reset(self):
        #restarts listening #should be coordinated be another class
        print('is resetting')
        self.set_listening(False)
        print('starting proccessing')
        self.pep_stream.startProcessing(stream_now = False)


    def stop(self):
        
        #stop litening to everything
        self.pep_stream.stopProcessing()
        self.set_listening(False)
        self.set_auto_life(True)

    def set_wit_token(self, token):
        self.pep_stream.set_wit_token(token)

