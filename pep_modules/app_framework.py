import qi
import json
import os
from tab_helpers import	tablet_coms_service

class app_framework:

	def __init__(self, session, app_route, console_printing = True):

		self.s = session
		try:
			#create an instance of MyFooService
			self.coms = tablet_coms_service(session)
			#register a callback for the coms module
			self.coms.tablet_to_pepper.connect(self._tablet_input_callback)
		except:
			print 'error in coms service'
			print Exception

		try:
			self.tabletService = session.service("ALTabletService")
		except:
			print 'Error in tablet service'
			print Exception


		self.tabletService.enableWifi()
		self.tabletService.showWebview("")

		#the function that accepts requests from the tablet
		self.app_route = app_route

		#boolian to set console printing
		self.console_printing = console_printing

		#probs should make path to app and arg
		self.path = os.getcwd()

		#set up for using a database
		DB = None


	def close(self):
		'''
		IMPORTANT! this mush be call whenever the framework is closed. 
		Pepper can get very confused
		'''
		#clean the tablet service

		self.tabletService.hideWebview()
		self.tabletService.cleanWebview()

		self.coms.disconnect()


	#----------------------------------------------------------------------------------------------------#
	#
	#											Tablet functions							
	#
	#----------------------------------------------------------------------------------------------------#

	def _tablet_input_callback(self, request):
		'''
		Is called whenever a message is recieved by javascript
		'''
		request = json.loads(request)
		
		if not self._private_routes(request):
			self.app_route(request)


	def _private_routes(self, request):
		'''
		A function that will intercept a request that is dealt with nativly, 
		e.g console logs and errors
		'''

		#Check if user has blocked console pipeing 
		if not self.console_printing:
			return False

		#check if thisi is a console output
		if request['route'] == 'CONSOLE':

			if request['request_type'] == 'LOG':
				print '\nMESSAGE LOGGED ON TABLET: ' + str(request['data'])

			if request['request_type'] == 'ERROR':
				data = request['data']
				print ('\nERROR LOGGED ON TABLET: ' + 
						str(data['line']) + ':' + str(data['col'])  + 
						'\n' + str(data['message']) + ' \nAt: ' + str(data['url']))

			return True
	
		else:	
			return False

	def send_to_tablet(self, message):
		'''
		Will send a message message or jsonified obect to a callback 
		that has been predefined in java-script on the tablet 
		'''
		self.coms.send_to_tablet(json.dumps(message))


	def display_page(self, template, style = None, script = None):
		
		#this will only work on linux and mac
		#this is a stupid way of doing it, i will change to path being an arg
		path = (self.path.replace('/data/home/nao/.local/share/PackageManager/', 'http://198.18.0.1/') + 
				'/' + template)

		self.tabletService.cleanWebview()
		self.tabletService.showWebview(path)

	def clear_tablet(self):
		'''
		Will clean the tablets memory, and leave the screan blank
		'''
		print('cleaning tablet')
		self.tabletService.hideWebview()
		self.tabletService.cleanWebview()

	#----------------------------------------------------------------------------------------------------#
	#
	#											DB Functions							
	#
	#----------------------------------------------------------------------------------------------------#

	#provide a path to the database
	def set_DB(self, path):
		'''
			Used to define the path to the DB that the app will use
		'''
		self.DB = None#create_engine(path)


	#pass a string and return the return 
	def DB_quirey(self, str):
		'''
			This is an untested function that will take an sql quirey as a string and return
			an array of responces
		'''
		return
		if not self.DB:
			print('use set_DB() to define a database')
			return None

		with self.DB.connect() as con:
			responce = con.execute(_parse_DB_quirey(str))
		return responce


	#escape chars in string
	def _Parse_DB_quirey(self, str):	
		#TODO
		return str

	def execute_JS(self, text):

		self.tabletService.executeJS(text)