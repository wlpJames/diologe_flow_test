
import qi
import time
import requests
import threading
from six.moves import queue
import wave
import struct
import numpy as np


# a function to access a stream of raw audio data

class Stream_to_wit(object):
	"""
    A class used to Stream audio directly from the pepper robot and bypassing ALAudio

    This class will only run on the pepper bot via ssh or within another app.

	...

	Atributes
	-------
	read the code

	Methods
	-------
	__init__(self, app, wit_token)
		inits, subscribes ect.
	
	startProcessing(self, seconds = none, stream_now = fasle)		
		starts intercepting audio

		If not given a value for seconds and stream now is set to true, This function will start streaming to wit in another thread, 

		if stream_now is set to false, this function will intercept audio to send to a given function but will wait for a call to start_streaming()

	def start_streaming(self):
		will start streaming if stream now has been set to fasle

	def stop_streaming(self):
		a function that will stop streaming but will allow the 
		audio processing to continue
		Put None into buffer to indicate the end of the stream

	stopProcessing(self)
		ends the session and stops processing

	getLastResponce(self)
		return json of the last recieved responce

	isProcessing(self)
		returns true if audio session is currently being processed

	set_audio_process_obj(self, obj)
		an optional object that can proccess the raw audio between its interception 
		and its being streamed to wit

		on init this function is none

		the object must have a function process which will be called on each frame of audio data.
		The process() function must accept only one argument, (the data), and shall return the 
		data in the same format in which it is given (an np.float32 array) 

    """

	def __init__(self, app, Min_buff_len = 30, api_token = 'MXL5CR3A43N6E65QWJSYWO2ESF5QIOQE'):
		"""
        Parameters
        ----------
        app : qi.Application
            pepper app object (qi.Application)

        Min_buff_len : int
        	default 30
        	the pep_streaming object will keep a buffer of this length to mitigate latancy between 
        	speach starting and the apropriate function calles from qi

        wit_token : string
        	access token supplied by wit (defualt is emplty acount set up by will)


            
        """
		super(Stream_to_wit, self).__init__()
		
		session = app.session

		# Get the service ALAudioDevice.
		self.audio_service = session.service("ALAudioDevice")
		self.framesCount=0
		self.module_name = "Stream_to_wit"

		self.audio_processing_obj = None

		#--------------for wit----------------#
		self.API_ENDPOINT = 'https://api.wit.ai/speech'
		self.ACCESS_TOKEN = wit_token

		#--Create a thread-safe buffer of audio data--#
		self._buff = queue.Queue()
		self.processing = False
		self.timerThread = None
		self.requestThread = None

		self.responce = None
		self.min_buff_len = Min_buff_len

		self.ignor_responce = None

		self.min_buff_len_withTouch = 10

		self.has_been_activated_by_touch = False
	

	def __del__(self): 
		if self.processing:
			self.stopProcessing()
		if self.timerThread:
			print('waiting to close timeing thread')
			self.timerThread.join()

	def startProcessing(self, seconds = None, stream_now = True):

		"""
        Parameters
        ----------
        seconds : qi.Application
            Time in seconds for the interception, if not supplied, module will continue to proccess on a separate thread (optional) 

        stream_now : boolean
			if set to true will start streaming for a set amount of seconds

        """

		print('am proccessing')

		#unset ignor responce in case of previous abortion
		self.ignor_responce = False


		if self.processing == True:
			print('already processing audio')
			return None

		if seconds >= 20:
			print('wit can\'t process an audio stream greater or equal to 20 seconds long')
			return None

		# ask for the front microphone signal sampled at 16kHz
		self.audio_service.setClientPreferences(self.module_name, 16000, 3, 0)
		self.audio_service.subscribe(self.module_name)
		self.processing = True

		if seconds != None and not stream_now:
			#start streaming in this thread and set timer in separarate thread
			self.timerThread = threading.Thread(target=self.timer, args=([seconds]))
			self.timerThread.start()
			self.sendToWit()
			self.timerThread.join()
			self.timerThread = None
		
		elif seconds == None and stream_now == True:
			print('started streaming from pep_stream')
			#send a wit request in a separate thread if no time value given
			if self.requestThread != None:
				print('waiting for request thread')
				self.requestThread.join()

			self.requestThread = threading.Thread(target=self.sendToWit)
			self.requestThread.start()
		

		return


	def start_streaming(self):
		#send a wit request in a separate thread
		if self.requestThread != None:
			print('waiting for request thread')
			self.requestThread.join()

		print('starting streaming')
		self.requestThread = threading.Thread(target=self.sendToWit)
		self.requestThread.start()
		pass


	def abort(self):
		'''a message given to terminate stream and ignor responce'''
		self.ignor_responce = True
		self.stopProcessing()

	def stop_streaming(self):
		'''
		a function that will stop streaming but will allow the 
		audio processing to continue
		'''
		#put None into buffer to indicate the end of the stream
		print('stopping streaming!!!')
		self._buff.put(None)
		
		print('none put in buffer')

		print(self.requestThread)



		if self.requestThread:
			self.requestThread.join()
			self.requestThread = None

		return

	def timer(self, seconds):
		time.sleep(seconds)
		self.stopProcessing(_called_by_timer = True)

	def stopProcessing(self, _called_by_timer = False):

		'''
		will stop proccessing audio data from qi

		'''

		if not self.processing:
			print('no processing running, perhaps it was timed out (max 20 secs)')
			return

		print('stopping processing!!!!')
		#unsubscribe to end calls to self.processRemote()
		self.audio_service.unsubscribe(self.module_name)

		#unset thread flag that allows processing data
		self.processing = False

		self.stop_streaming()



	def processRemote(self, nbOfChannels, nbOfSamplesByChannel, timeStamp, inputBuffer):
		'''
		a callback function? referenced as process in qi documentation -- functionally seems to be the callback for port audio

		'''
		#add input to buffer
		if self.processing == True:
			if self.audio_processing_obj != None: 
				try:
					x = 1
					#convert to vactoe -1 > x > 1
					inputBuffer = np.asarray(struct.unpack('<{}h'.format(nbOfSamplesByChannel), inputBuffer)).astype(np.float32) / 3276.7
					
					#send to a proccessing class
					inputBuffer = self.audio_processing_obj.process(inputBuffer)

					#convert back to byte string
					inputBuffer = struct.pack('<{}h'.format(len(inputBuffer)), * (inputBuffer * 3276.7))

				except Exception as e:
					print('there is an issue with the given sound processing class, please check the protocal')
					print(e)

			#add input buffer to our self managed buffer
			self._buff.put(inputBuffer)
	
			#keep buffer size to minimum
			while self._buff.qsize() > self.min_buff_len:
				chunk = self._buff.get()


	def set_audio_process_obj(self, obj):
		"""
        Parameters
        ----------
        obj : object pointer
            a pointer to an object with process() func which accepts input data and outputs data in the same format
        """
		try:
			self.audio_processing_obj = obj
			return True

		except:
			print('this is not a valid class')
			return False


	def sendToWit(self):

		'''
		begins the streaming seassion with wit
		'''
		
		headers = {'authorization': 'Bearer ' + self.ACCESS_TOKEN, 
			'content-type':  "audio/raw; encoding=signed-integer; bits=16; rate=16000; endian=little",
			'Transfer-Encoding': 'chunked',
			'context': 'context-object'}

		#begin chunked request
		start = time.time()

		try:
			responce = requests.post(self.API_ENDPOINT, headers = headers, data = self.generator())
			if not self.ignor_responce:
				self.responce = responce

		except Exception as e:
			print(e)
			self.responce = None

		print('time taken since steaming started responce: ', time.time() - start)


	def generator(self):

		#remove all but x buffer chunks as we only want those that 
		val = (self.min_buff_len_withTouch if self.has_been_activated_by_touch else self.min_buff_len)
		while self._buff.qsize() > val:
			chunk = self._buff.get()

		while True:
			'''
			Use a blocking get() to ensure there's at least one chunk of
			data, and stop iteration if the chunk is None, indicating the
			end of the audio stream.
			'''
			chunk = self._buff.get()
			if chunk is None:
				print 'none found'
				return 

			#iterate through sections of chunks. try divide by 30


			yield chunk


	def getLastResponce(self):
		#return json of the last recieved responce
		return self.responce


	def isProcessing(self):
		return self.processing

	def set_wit_token(self, token):
		self.ACCESS_TOKEN = token
