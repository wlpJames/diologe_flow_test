import numpy as np
import struct

class speach_detect:
	"""
    a class that can take a stream of audio and discern where an instance of speak is started, ongoing or finished

	...

	Atributes
	-------
	read the code

	Methods
	-------
	__init__(self, Threshold, Silence_Threshold, SampleRate, activation = 'voice')

		Sets definitions of silence, noise and waiting times
	
	def process(self, input_buffer):
		Called by the pep_stream class on each audio buffer

	def gain(self, signal, gain):
		Multiplys the signal by the gain value, either boosting or lowering the levels
		
	def startListening(self):
		Will set the stage to listening if currently inactave

	def speach_started(self):
		Will set the stage to speaking

	def getStage(self):
		Returns the current stage

	def reset(self):
		will reset the object
    """


	def __init__(self, Threshold, Time_period, abortion_time_period = 1, SampleRate = 16000, Buffer_len = 1365):
		"""
        Parameters
        ----------
        Threshold : int
            the defanition of noise/speaking levels (0 > x > 1)

        Silence_Threshhold : int (seconds)
        	the period of time by which silence shall be counted as end of speach

        SampleRate : int
        	the sample rate of the expected audio buffer

        activation : string
        	'voice' will listen from the start untill the end and decide open speach times
			'command' Will wait for commands to decide when speach has started -- will actually change this i think
            
        """
		
		#homemade python enum
		self.inactive = 0
		self.speaking = 1
		self.finished = 2
		self.aborted = 3

		self.stage = self.inactive

		self.sample_rate = SampleRate
		self.buffer_len = Buffer_len
		self.tot_samples_proccessed = 0	

		#the level that serves as the definition of noise and silence (0 > x > 1) 
		self.noise_threshold = Threshold

		#time period in samples
		self.time_period = Time_period * SampleRate
		self.abortion_time_period = abortion_time_period * SampleRate
		self.levels_buffer = []



	def process(self, input_buffer):
		"""
        Parameters
        ----------
		input_buffer : [float]
        """
		if self.stage != self.finished and self.stage != self.inactive:
			#this could be in sepate thread
			self.find_stages(input_buffer)

		#returns input in the form it was given
		return self.gain(input_buffer, 1.5)

	
	def gain(self, signal, gain):
		""" 
        Parameters
        ----------
		Signal : [float]

		gain : float
			The gain coeficiant 
        """
		return signal * gain


	def startListening(self):

		if self.stage == self.inactive:
			print ('pepper is waiting for voice')
			self.stage = self.waiting

		else:
			print('stage is not inactive')


	def speach_started(self):

		print('speach commanded to start')
		self.stage = self.speaking



	def find_stages(self, signal):

		#for debug
		#print ('current stage is: {}'.format(self.stage))
		#find avarage levels over a given period of time
		level = self.calcLevel(signal)	
		print('level: {}'.format(level))	
		if self.stage == self.speaking:

			self.tot_samples_proccessed += len(signal)

			#check if there is silence or if there is noise

			if level <= self.noise_threshold and self.tot_samples_proccessed > self.time_period:

				#either abort or signal end of speach
				if (self.tot_samples_proccessed < self.abortion_time_period + self.buffer_len):
					print ('ABORTED')
					self.stage = self.aborted

				else:
					print('finished ')
					self.stage = self.finished


	
	def calcLevel(self, data):
		"""
		Calculate noise level
		"""

		#push new level on to end of array 
		self.levels_buffer = np.append(self.levels_buffer, data)

		#and dispose of the oldest level if buffer s of length
		if len(self.levels_buffer) > self.time_period:
			self.levels_buffer = self.levels_buffer[self.buffer_len:]

		avv_levels = np.sum(map(abs, self.levels_buffer)) / len(self.levels_buffer)

		return avv_levels * 2



	def getStage(self):
		return self.stage

	def reset(self):
		#resets values for new quirey
		self.levels_buffer = []
		self.stage = self.inactive
		self.tot_samples_proccessed = 0


