// here is a callback that is defined for recieve a responce from app.py
function callback_func(data) {
	document.getElementById('display_area').innerHTML = data
}

// this will send a message -- look for (request['route'] == 'index/JSON') in example_app.py
function send_data_request() {
	request_type = 'GET';
	route = 'index/JSON';
	data = {
		c: "This is a message"
	};
	pepper_request(request_type, route, data, callback_func);
}

// here we seng a request to change page -- look for (request['route'] == 'page_2') in example_app.py)
function new_page() {
	request_type = 'GET'
	route = 'page_2'
	data = ''
	pepper_request(request_type, route, data)
}